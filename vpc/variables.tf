variable "vpc_cidr_block" {
  type = string
}

variable "subnet01_cidr_block" {
  type = string
}

variable "availability_zone" {
  type = string
}

variable "myvpc_id" {
  type = string
}