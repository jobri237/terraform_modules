# resource "aws_vpc" "devsecops_vpc" {
#   cidr_block       = var.vpc_cidr_block
#   instance_tenancy = "default"

#   tags = {
#     Name = "devsecops_vpc"
#   }
# }


resource "aws_internet_gateway" "dojo_gw" {
  vpc_id = var.myvpc_id

  tags = {
    Name = "main_gw"
  }
}

resource "aws_route_table" "dojo_routeTable" {
  vpc_id = var.myvpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dojo_gw.id
  }

  tags = {
    Name = "defect_dojo"
  }
}

resource "aws_main_route_table_association" "a" {
  vpc_id         = var.myvpc_id
  route_table_id = aws_route_table.dojo_routeTable.id
}


