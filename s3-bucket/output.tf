output "s3_bucket" {
  value = aws_s3_bucket.s3_bucket.bucket
}

output "region" {
  value = aws_s3_bucket.s3_bucket.region
}

output "state_name" {
  value = aws_dynamodb_table.statelock-dynamodb-table.name
}