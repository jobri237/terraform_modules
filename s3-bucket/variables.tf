variable "s3_bucket_name" {
  type        = string
  description = "Your s3 bucket name"
}

variable "environment" {
  type        = string
  default     = "dev"
  description = "this is the dev environment"
}

variable "state_name" {
  type        = string
  description = "Dynamo Db state table"
}