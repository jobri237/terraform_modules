output "vpc_id" {
  value = aws_security_group.allow_tls.vpc_id
}

output "cidr_block" {
  value = aws_security_group.allow_tls.vpc_id
}

output "sg_name" {
  value = aws_security_group.allow_tls.name
}

output "sg_id" {
  value = aws_security_group.allow_tls.id
}