variable "aws_region" {
  type        = string
  description = "AWS Region that will host your resources"
}

variable "cidr_block" {
  type = string
}

variable "availability_zone" {
  type = string
}

variable "subnet01_cidr_block" {
  type = string
}

variable "sg_name" {
  type = string
}