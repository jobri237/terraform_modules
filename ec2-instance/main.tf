resource "aws_network_interface" "eth0" {
  subnet_id = var.my_subnet

  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_instance" "my_ec2" {
  ami                         = var.ami_id # us-west-2
  instance_type               = var.ec2_type
  key_name                    = var.key_name
  vpc_security_group_ids      = [var.sg_id]
  subnet_id                   = var.my_subnet
  associate_public_ip_address = true

  root_block_device {
    volume_size = var.volume_size
    volume_type = "gp3"
    delete_on_termination = true
  }

  tags = {
    Name = var.instance_name
  }

  #   credit_specification {
  #     cpu_credits = "unlimited"
  #   }
}