variable "ami_id" {
  type    = string
  default = "ami-0cf2b4e024cdb6960"
}

variable "ec2_type" {
  type    = string
  default = "t2.micro"
}

variable "my_subnet" {
  type = string
}

variable "sg_id" {
  type = string
}

variable "instance_name" {
  type        = string
  description = "The name of the ec2 instance"
}

variable "key_name" {
  type = string
}

variable "volume_size" {
  type = string
  default = "8"
}
