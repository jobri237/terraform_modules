output "AWS_Region" {
  value = var.aws_region
}

# output "defectDojo_url" {
#   value = "http://${module.dojo_instance.public_ip}:8080" 
# }

output "gitlabRunner_public_ip" {
  value = module.runner_instance.public_ip
}

# output "public_dns" {
#   value = module.ec2_instance.public_dns
# }