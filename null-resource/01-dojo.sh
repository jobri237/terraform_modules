#! /bin/bash

mkdir $HOME/files
cd $HOME/files

# Clone DefectDojo repository
git clone https://gitlab.com/jobri237/defectdojo.git

cd defectdojo

# #Disbling postgresql docker container 
# sed -i '/postgres:/,/volumes:/ { /  - postgres-redis/d }' docker-compose.yml

# #Setting Connection to postgresql instance in AWS
# sed -i 's/defectdojo:defectdojo@postgres/sne:ddojo237@django.db.backends.postgresql/g' docker/environments/postgres-redis.env

# Run the build script to install defectDojo
chmod u+x dc-build.sh
./dc-build.sh

# Start DefectDojo
chmod u+x dc-up.sh 
./dc-up.sh postgres-redis

echo "========================Defect Dojo Admin Password========================\n"
docker compose logs initializer | grep "Admin password:"

