#!/bin/bash

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner

sudo gitlab-runner register --non-interactive --token="$RUNNER_TOKEN"  --name="aws-ec2" --executor="docker" --docker-image="python" --url="https://gitlab.com" --request-concurrency="3" --docker-services-limit="3"

systemctl restart gitlab-runner