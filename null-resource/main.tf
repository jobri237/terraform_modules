resource "null_resource" "run-scripts" {
  connection {
    type = "ssh"
    user = "ubuntu"
    host = var.public_ip
    private_key = "${file("private-key")}"
  }

# works in local env
  # provisioner "file" {
  #   source = "null-resource/00-docker.sh"
  #   destination = "/tmp/00-docker.sh"
  # }
  # provisioner "file" {
  #   source = "null-resource/${var.script}"
  #   destination = "/tmp/${var.script}"
  # }

  provisioner "file" {
    source = "00-docker.sh"
    destination = "/tmp/00-docker.sh"
  }
  provisioner "file" {
    source = var.script
    destination = "/tmp/${var.script}"
  }

  provisioner "remote-exec" {
    inline = [ 
        "file /tmp/00-docker.sh",
        "chmod +x /tmp/00-docker.sh",
        "/tmp/00-docker.sh"
     ]
  }
  provisioner "remote-exec" {
    inline = [ 
        "file tmp/${var.script}",
        "chmod +x /tmp/${var.script}",
        "/tmp/${var.script}"
     ]
  }
}