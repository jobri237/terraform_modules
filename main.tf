terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.aws_region
}

module "devsecops_vpc" {
  source              = "./vpc"
  vpc_cidr_block      = var.cidr_block
  subnet01_cidr_block = var.subnet01_cidr_block
  availability_zone   = var.availability_zone
  myvpc_id = "vpc-0c6ac57b99a4af17d"
}

module "security_group" {
  source     = "./security-group"
  vpc_id     = module.devsecops_vpc.vpc_id
  depends_on = [module.devsecops_vpc]
  sg_name    = var.sg_name
}

module "subnet01" {
  source              = "./subnet"
  vpc_id              = module.devsecops_vpc.vpc_id
  availability_zone   = var.availability_zone
  subnet01_cidr_block = var.subnet01_cidr_block
  depends_on          = [module.devsecops_vpc]
}

module "ssh-key" {
  source = "./key-pair"
}

# module "dojo_instance" {
#   source        = "./ec2-instance"
#   instance_name = "Defect-Dojo"
#   ec2_type      = "t2.medium"
#   volume_size   = "15"
#   my_subnet     = module.subnet01.subnet_id
#   sg_id         = module.security_group.sg_id
#   key_name      = module.ssh-key.key
#   depends_on    = [module.devsecops_vpc, module.security_group, module.subnet01]
# }

# module "dojo-script" {
#   source = "./null-resource"
#   script = "01-dojo.sh"
#   public_ip = module.dojo_instance.public_ip
# }


module "runner_instance" {
  source        = "./ec2-instance"
  instance_name = "Gitlab-runner"
  my_subnet     = module.subnet01.subnet_id
  sg_id         = module.security_group.sg_id
  key_name      = module.ssh-key.key
  depends_on    = [module.devsecops_vpc, module.security_group, module.subnet01]
}
# module "runner-script" {
#   source = "./null-resource"
#   script = "02-runner.sh"
#   public_ip = module.runner_instance.public_ip
# }

