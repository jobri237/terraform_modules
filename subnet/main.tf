resource "aws_subnet" "my_subnet" {
  vpc_id            = var.vpc_id
  cidr_block        = var.subnet01_cidr_block
  availability_zone = var.availability_zone

  tags = {
    Name = "subnet-01"
  }
}
