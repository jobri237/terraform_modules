variable "availability_zone" {
  type = string
}

variable "subnet01_cidr_block" {
  type = string
}

variable "vpc_id" {
  type = string
}